LOAD CSV WITH HEADERS FROM "file:/Users/sophia/Downloads/courseData/nodeSample.csv" AS row
CREATE (:Node {objectId: row.objectId});

LOAD CSV WITH HEADERS FROM "file:/Users/sophia/Downloads/courseData/objectAcqYearSample.csv" AS row
CREATE (:Year {acqyear: row.acqyear});

LOAD CSV WITH HEADERS FROM "file:/Users/sophia/Downloads/courseData/objectCountrySample.csv" AS row
CREATE (:Country {countryname: row.countryname});

LOAD CSV WITH HEADERS FROM "file:/Users/sophia/Downloads/courseData/objectCreatorSample.csv" AS row
CREATE (:Creator {creatorname: row.creatorname});

LOAD CSV WITH HEADERS FROM "file:/Users/sophia/Downloads/courseData/objectCreditSample.csv" AS row
CREATE (:Credit {creditname: row.creditname});

LOAD CSV WITH HEADERS FROM "file:/Users/sophia/Downloads/courseData/objectDateSample.csv" AS row
CREATE (:Date {objectdate: row.objectdate});

LOAD CSV WITH HEADERS FROM "file:/Users/sophia/Downloads/courseData/objectTitleSample.csv" AS row
CREATE (:Title {titlename: row.titlename});

LOAD CSV WITH HEADERS FROM "file:/Users/sophia/Downloads/courseData/objectTopicSample.csv" AS row
CREATE (:Topic {topicname: row.topicname});

LOAD CSV WITH HEADERS FROM "file:/Users/sophia/Downloads/courseData/objectTypeSample.csv" AS row
CREATE (:Type {typename: row.typename});

LOAD CSV WITH HEADERS FROM "file:/Users/sophia/Downloads/courseData/edgeSample.csv" AS row
CREATE (:Edge {from: row.objectid, to: row.attributeid});

MATCH(d:Edge) WITH d.from AS f, d.to AS t MATCH(n1:Node),(n2:Country) WHERE n1.objectId=f AND n2.countryname=t	CREATE (n1)-[r:ToCountry{from:f, to:t}]->(n2) RETURN r;

MATCH(d:Edge) WITH d.from AS f, d.to AS t MATCH(n1:Node),(n2:Creator) WHERE n1.objectId=f AND n2.creatorname=t	CREATE (n1)-[r:ToCreator{from:f, to:t}]->(n2) RETURN r;

MATCH(d:Edge) WITH d.from AS f, d.to AS t MATCH(n1:Node),(n2:Date) WHERE n1.objectId=f AND n2.objectdate=t	CREATE (n1)-[r:ToDate{from:f, to:t}]->(n2) RETURN r;

MATCH(d:Edge) WITH d.from AS f, d.to AS t MATCH(n1:Node),(n2:Title) WHERE n1.objectId=f AND n2.titlename=t	CREATE (n1)-[r:ToTitle{from:f, to:t}]->(n2) RETURN r;

MATCH(d:Edge) WITH d.from AS f, d.to AS t MATCH(n1:Node),(n2:Topic) WHERE n1.objectId=f AND n2.topicname=t CREATE (n1)-[r:ToTopic{from:f, to:t}]->(n2) RETURN r;

MATCH(d:Edge) WITH d.from AS f, d.to AS t MATCH(n1:Node),(n2:Type) WHERE n1.objectId=f AND n2.typename=t CREATE (n1)-[r:ToType{from:f, to:t}]->(n2) RETURN r;

MATCH(d:Edge) WITH d.from AS f, d.to AS t MATCH(n1:Node),(n2:Year) WHERE n1.objectId=f AND n2.acqyear=t CREATE (n1)-[r:ToYear{from:f, to:t}]->(n2) RETURN r;

/*an example*/
MATCH p=()-[r:ToDate]->() where r.from = 'chndm_1896-1-103' or r.from = 'chndm_1896-1-119' RETURN p


/*Test 1*/
MATCH (p1:Node {objectId: 'chndm_1896-1-103'})
MATCH (p2:Node {objectId: 'chndm_1896-1-119'})
RETURN algo.linkprediction.adamicAdar(p1, p2, {relationshipQuery: "ToDate"}) AS score;

/*Test 2*/
MATCH (p1:Node {objectId: 'chndm_1896-1-103'})
MATCH (p2:Node {objectId: 'chndm_1896-1-119'})
RETURN algo.linkprediction.adamicAdar(p1, p2) AS score;



